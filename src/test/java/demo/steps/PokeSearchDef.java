package demo.steps;

import demo.controller.PokemonController;
import demo.pages.ArticlePages;
import demo.pages.GoogleHomePages;
import demo.pages.GoogleSearchPages;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import org.junit.Assert;


public class PokeSearchDef {

    private final PokemonController pokemonController = new PokemonController();
    private final GoogleHomePages homePages = new GoogleHomePages();
    private final GoogleSearchPages searchPages = new GoogleSearchPages();
    private final ArticlePages articlePages = new ArticlePages();

    @Given("User get pokemon {string} and id from API")
    public void userGetPokemonNameAndId(String name) {
        pokemonController.getPokemon(name);
    }

    @When("User open Google Search homepage")
    public void userOpenGoogleSearchHomepage() {
        homePages.openHomePage();
    }

    @And("User input Search {string}")
    public void userInputSearch(String keyword) {
        homePages.inputSearch(keyword);
    }

    @And("User press Enter Button")
    public void userPressEnterButton() {
        homePages.pressEnter();
    }

    @And("User Click The First Result")
    public void userClickTheFirstResult() {
        searchPages.clickFirstSearch();
    }

    @And("User see article with title {string}")
    public void userSeeArticleWithTitle(String title) {
        String actual_title = articlePages.identifyName();
        Assert.assertEquals(title, actual_title);
    }

    @And("User Get the name {string} and id {string} from Wikipedia")
    public void userGetTheNameAndIdFromWikipedia(String name, String number) {
       articlePages.identifyName();
       articlePages.identifyNumber();
    }

    @Then("User perform Assertion pokemon {string}")
    public void userPerformAssertionPokemon(String name) {
        String webName = articlePages.identifyName().toLowerCase();
        String webID = articlePages.identifyNumber();

        Response response = pokemonController.getPokemon(name);
        String apiName = response.path("name");
        String apiID = response.path("id");

        Assert.assertTrue(webID.contains(apiID));
        Assert.assertTrue(webName.contains(apiName));
    }


}
