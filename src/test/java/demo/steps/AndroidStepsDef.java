package demo.steps;

import demo.pages.RefreshPage;
import demo.pages.ToDoHomepages;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

public class AndroidStepsDef {
    ToDoHomepages toDoHomepages = new ToDoHomepages();
    RefreshPage refreshPage = new RefreshPage();

    @Given("User is on to do list page")
    public void userIsOnToDoListPage() {
        boolean result = toDoHomepages.isOnPage();
        Assert.assertTrue(result);
    }

    @When("User click options on to do list")
    public void userClickOptionsOnToDoList() {
        toDoHomepages.toDoOptions();
        refreshPage.refreshPage();
    }

    @And("User click refresh page")
    public void userClickRefreshPage() {
    }

    @Then("Page Displayed")
    public void pageDisplayed() {
    }
}
