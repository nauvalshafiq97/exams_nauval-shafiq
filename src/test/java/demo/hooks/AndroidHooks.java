package demo.hooks;

import demo.driver.AndroidDriverInstance;
import io.cucumber.java.After;
import io.cucumber.java.Before;

public class AndroidHooks {

    @Before(value = "@Android_1")
    public void initializeAndroidDriver(){
        AndroidDriverInstance.initialize();
    }

    @After(value = "@Android")
    public void quitAndroidDriver(){
        AndroidDriverInstance.quit();
    }
}
