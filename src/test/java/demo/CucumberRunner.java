package demo;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/features/WebAPIPokemon.feature",
        plugin = {"html:build/result"},
        stepNotifications = true,
        tags = {"@Web"}
)
public class CucumberRunner {
}
