package demo.pages;

import demo.driver.WebDriverInstance;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GoogleSearchPages {

    public void clickFirstSearch(){
        WebElement element = WebDriverInstance.webDriver.findElement(By.xpath("//div/a"));
        element.click();
    }
}
