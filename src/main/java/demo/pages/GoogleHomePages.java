package demo.pages;

import demo.driver.WebDriverInstance;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

public class GoogleHomePages {

    public void openHomePage(){
        WebDriverInstance.webDriver.get("https://www.google.com/");
    }

    public void inputSearch(String keyword){
        WebElement inputSearch = WebDriverInstance.webDriver.findElement(By.xpath("//input[@name='q']"));
        inputSearch.sendKeys(keyword);
    }

    public void pressEnter(){
        WebElement enterSearch = WebDriverInstance.webDriver.findElement(By.xpath("//input[@name='q']"));//finding by xpath
        enterSearch.sendKeys(Keys.ENTER);
    }
}
