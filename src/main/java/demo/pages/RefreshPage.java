package demo.pages;

import demo.driver.AndroidDriverInstance;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.By;

public class RefreshPage {

    public void refreshPage(){
        AndroidElement element = AndroidDriverInstance.androidDriver.findElement(By.className("android.widget.TextView"));
        element.click();
    }
}
