package demo.pages;

import demo.driver.WebDriverInstance;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class ArticlePages {

    public String identifyName(){
        WebElement name = WebDriverInstance.webDriver.findElement(By.id("firstHeading"));
        return name.getText();
    }

    public String identifyNumber(){
        WebElement number = WebDriverInstance.webDriver.findElement(By.xpath("//big/b/abbr"));
        return number.getText();
    }
}
