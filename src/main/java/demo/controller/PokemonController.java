package demo.controller;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class PokemonController {

    public Response getPokemon(String name){
        Response response = RestAssured
                .given()
                .baseUri("http://pokeapi.co") //base URL
                .basePath("/api/v2/pokemon") //base path
                .header("Content-type", "application/json") //create header
                .header("Accept", "application/json")
                .get("/"+name); //get content

        return response;
    }
}
