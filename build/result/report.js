$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/WebAPIPokemon.feature");
formatter.feature({
  "name": "Search Pokemon",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Web"
    }
  ]
});
formatter.scenario({
  "name": "Search Pikachu from Google and Perform Assertion from Google and API",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Web"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User get pokemon \"pikachu\" and id from API",
  "keyword": "Given "
});
formatter.match({
  "location": "demo.steps.PokeSearchDef.userGetPokemonNameAndId(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User open Google Search homepage",
  "keyword": "When "
});
formatter.match({
  "location": "demo.steps.PokeSearchDef.userOpenGoogleSearchHomepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input Search \"Pikachu Wikipedia Indonesia\"",
  "keyword": "And "
});
formatter.match({
  "location": "demo.steps.PokeSearchDef.userInputSearch(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User press Enter Button",
  "keyword": "And "
});
formatter.match({
  "location": "demo.steps.PokeSearchDef.userPressEnterButton()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User Click The First Result",
  "keyword": "And "
});
formatter.match({
  "location": "demo.steps.PokeSearchDef.userClickTheFirstResult()"
});
formatter.result({
  "error_message": "org.openqa.selenium.ElementClickInterceptedException: element click intercepted: Element \u003ca jsname\u003d\"BKxS1e\" class\u003d\"gyPpGe\" role\u003d\"link\" tabindex\u003d\"0\" jsaction\u003d\"i3viod\" data-ved\u003d\"0ahUKEwiI9vaA24_pAhVEX30KHVhKBmcQ67oDCAQ\"\u003e...\u003c/a\u003e is not clickable at point (66, 86). Other element would receive the click: \u003cdiv\u003e...\u003c/div\u003e\n  (Session info: chrome\u003d81.0.4044.129)\nBuild info: version: \u00273.141.59\u0027, revision: \u0027e82be7d358\u0027, time: \u00272018-11-14T08:17:03\u0027\nSystem info: host: \u0027dnid0661l01.local\u0027, ip: \u0027fe80:0:0:0:1cf4:b004:3e21:8c0a%en0\u0027, os.name: \u0027Mac OS X\u0027, os.arch: \u0027x86_64\u0027, os.version: \u002710.15.4\u0027, java.version: \u00271.8.0_251\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities {acceptInsecureCerts: false, browserName: chrome, browserVersion: 81.0.4044.129, chrome: {chromedriverVersion: 81.0.4044.69 (6813546031a4b..., userDataDir: /var/folders/1v/px0r5h4x0jz...}, goog:chromeOptions: {debuggerAddress: localhost:57878}, javascriptEnabled: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: MAC, platformName: MAC, proxy: Proxy(), setWindowRect: true, strictFileInteractability: false, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}, unhandledPromptBehavior: dismiss and notify, webauthn:virtualAuthenticators: true}\nSession ID: 415222660cb538f8688f2c5853087322\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.createException(W3CHttpResponseCodec.java:187)\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:122)\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:49)\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:158)\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:552)\n\tat org.openqa.selenium.remote.RemoteWebElement.execute(RemoteWebElement.java:285)\n\tat org.openqa.selenium.remote.RemoteWebElement.click(RemoteWebElement.java:84)\n\tat demo.pages.GoogleSearchPages.clickFirstSearch(GoogleSearchPages.java:14)\n\tat demo.steps.PokeSearchDef.userClickTheFirstResult(PokeSearchDef.java:44)\n\tat ✽.User Click The First Result(file:///Users/shafiqn/IdeaProjects/Exam_Nauval-Shafiq/src/test/resources/features/WebAPIPokemon.feature:8)\n",
  "status": "failed"
});
formatter.step({
  "name": "User see article with title \"Pikachu\"",
  "keyword": "And "
});
formatter.match({
  "location": "demo.steps.PokeSearchDef.userSeeArticleWithTitle(java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User Get the name \"Pikachu\" and id \"25\" from Wikipedia",
  "keyword": "And "
});
formatter.match({
  "location": "demo.steps.PokeSearchDef.userGetTheNameAndIdFromWikipedia(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User perform Assertion pokemon \"Pikachu\"",
  "keyword": "Then "
});
formatter.match({
  "location": "demo.steps.PokeSearchDef.userPerformAssertionPokemon(java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Search Charizard from Google and Perform Assertion from Google and API",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Web"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User get pokemon \"charizard\" and id from API",
  "keyword": "Given "
});
formatter.match({
  "location": "demo.steps.PokeSearchDef.userGetPokemonNameAndId(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User open Google Search homepage",
  "keyword": "When "
});
formatter.match({
  "location": "demo.steps.PokeSearchDef.userOpenGoogleSearchHomepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input Search \"Charizard Wikipedia Indonesia\"",
  "keyword": "And "
});
formatter.match({
  "location": "demo.steps.PokeSearchDef.userInputSearch(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User press Enter Button",
  "keyword": "And "
});
formatter.match({
  "location": "demo.steps.PokeSearchDef.userPressEnterButton()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User Click The First Result",
  "keyword": "And "
});
formatter.match({
  "location": "demo.steps.PokeSearchDef.userClickTheFirstResult()"
});
formatter.result({
  "error_message": "org.openqa.selenium.ElementClickInterceptedException: element click intercepted: Element \u003ca jsname\u003d\"BKxS1e\" class\u003d\"gyPpGe\" role\u003d\"link\" tabindex\u003d\"0\" jsaction\u003d\"i3viod\" data-ved\u003d\"0ahUKEwjEvPOC24_pAhXdILcAHSBzDdUQ67oDCAQ\"\u003e...\u003c/a\u003e is not clickable at point (66, 86). Other element would receive the click: \u003cdiv\u003e...\u003c/div\u003e\n  (Session info: chrome\u003d81.0.4044.129)\nBuild info: version: \u00273.141.59\u0027, revision: \u0027e82be7d358\u0027, time: \u00272018-11-14T08:17:03\u0027\nSystem info: host: \u0027dnid0661l01.local\u0027, ip: \u0027fe80:0:0:0:1cf4:b004:3e21:8c0a%en0\u0027, os.name: \u0027Mac OS X\u0027, os.arch: \u0027x86_64\u0027, os.version: \u002710.15.4\u0027, java.version: \u00271.8.0_251\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities {acceptInsecureCerts: false, browserName: chrome, browserVersion: 81.0.4044.129, chrome: {chromedriverVersion: 81.0.4044.69 (6813546031a4b..., userDataDir: /var/folders/1v/px0r5h4x0jz...}, goog:chromeOptions: {debuggerAddress: localhost:57929}, javascriptEnabled: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: MAC, platformName: MAC, proxy: Proxy(), setWindowRect: true, strictFileInteractability: false, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}, unhandledPromptBehavior: dismiss and notify, webauthn:virtualAuthenticators: true}\nSession ID: c40101cb277c286fef1f3cd73b1e864d\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.createException(W3CHttpResponseCodec.java:187)\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:122)\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:49)\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:158)\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:552)\n\tat org.openqa.selenium.remote.RemoteWebElement.execute(RemoteWebElement.java:285)\n\tat org.openqa.selenium.remote.RemoteWebElement.click(RemoteWebElement.java:84)\n\tat demo.pages.GoogleSearchPages.clickFirstSearch(GoogleSearchPages.java:14)\n\tat demo.steps.PokeSearchDef.userClickTheFirstResult(PokeSearchDef.java:44)\n\tat ✽.User Click The First Result(file:///Users/shafiqn/IdeaProjects/Exam_Nauval-Shafiq/src/test/resources/features/WebAPIPokemon.feature:19)\n",
  "status": "failed"
});
formatter.step({
  "name": "User see article with title \"Charizard\"",
  "keyword": "And "
});
formatter.match({
  "location": "demo.steps.PokeSearchDef.userSeeArticleWithTitle(java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User Get the name \"Charizard\" and id \"25\" from Wikipedia",
  "keyword": "And "
});
formatter.match({
  "location": "demo.steps.PokeSearchDef.userGetTheNameAndIdFromWikipedia(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User perform Assertion pokemon \"Charizard\"",
  "keyword": "Then "
});
formatter.match({
  "location": "demo.steps.PokeSearchDef.userPerformAssertionPokemon(java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Search Bulbasaur from Google and Perform Assertion from Google and API",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Web"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User get pokemon \"bulbasaur\" and id from API",
  "keyword": "Given "
});
formatter.match({
  "location": "demo.steps.PokeSearchDef.userGetPokemonNameAndId(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User open Google Search homepage",
  "keyword": "When "
});
formatter.match({
  "location": "demo.steps.PokeSearchDef.userOpenGoogleSearchHomepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input Search \"Bulbasaur Wikipedia Indonesia\"",
  "keyword": "And "
});
formatter.match({
  "location": "demo.steps.PokeSearchDef.userInputSearch(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User press Enter Button",
  "keyword": "And "
});
formatter.match({
  "location": "demo.steps.PokeSearchDef.userPressEnterButton()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User Click The First Result",
  "keyword": "And "
});
formatter.match({
  "location": "demo.steps.PokeSearchDef.userClickTheFirstResult()"
});
formatter.result({
  "error_message": "org.openqa.selenium.ElementClickInterceptedException: element click intercepted: Element \u003ca jsname\u003d\"BKxS1e\" class\u003d\"gyPpGe\" role\u003d\"link\" tabindex\u003d\"0\" jsaction\u003d\"i3viod\" data-ved\u003d\"0ahUKEwjR2-GE24_pAhVNbysKHZuACw8Q67oDCAQ\"\u003e...\u003c/a\u003e is not clickable at point (66, 86). Other element would receive the click: \u003cdiv\u003e...\u003c/div\u003e\n  (Session info: chrome\u003d81.0.4044.129)\nBuild info: version: \u00273.141.59\u0027, revision: \u0027e82be7d358\u0027, time: \u00272018-11-14T08:17:03\u0027\nSystem info: host: \u0027dnid0661l01.local\u0027, ip: \u0027fe80:0:0:0:1cf4:b004:3e21:8c0a%en0\u0027, os.name: \u0027Mac OS X\u0027, os.arch: \u0027x86_64\u0027, os.version: \u002710.15.4\u0027, java.version: \u00271.8.0_251\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities {acceptInsecureCerts: false, browserName: chrome, browserVersion: 81.0.4044.129, chrome: {chromedriverVersion: 81.0.4044.69 (6813546031a4b..., userDataDir: /var/folders/1v/px0r5h4x0jz...}, goog:chromeOptions: {debuggerAddress: localhost:57969}, javascriptEnabled: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: MAC, platformName: MAC, proxy: Proxy(), setWindowRect: true, strictFileInteractability: false, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}, unhandledPromptBehavior: dismiss and notify, webauthn:virtualAuthenticators: true}\nSession ID: c6f25dcdffdb03051c68e553644db594\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.createException(W3CHttpResponseCodec.java:187)\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:122)\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:49)\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:158)\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:552)\n\tat org.openqa.selenium.remote.RemoteWebElement.execute(RemoteWebElement.java:285)\n\tat org.openqa.selenium.remote.RemoteWebElement.click(RemoteWebElement.java:84)\n\tat demo.pages.GoogleSearchPages.clickFirstSearch(GoogleSearchPages.java:14)\n\tat demo.steps.PokeSearchDef.userClickTheFirstResult(PokeSearchDef.java:44)\n\tat ✽.User Click The First Result(file:///Users/shafiqn/IdeaProjects/Exam_Nauval-Shafiq/src/test/resources/features/WebAPIPokemon.feature:29)\n",
  "status": "failed"
});
formatter.step({
  "name": "User see article with title \"Bulbasaur\"",
  "keyword": "And "
});
formatter.match({
  "location": "demo.steps.PokeSearchDef.userSeeArticleWithTitle(java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User Get the name \"Bulbasaur\" and id \"25\" from Wikipedia",
  "keyword": "And "
});
formatter.match({
  "location": "demo.steps.PokeSearchDef.userGetTheNameAndIdFromWikipedia(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User perform Assertion pokemon \"Bulbasaur\"",
  "keyword": "Then "
});
formatter.match({
  "location": "demo.steps.PokeSearchDef.userPerformAssertionPokemon(java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Search Mewtwo from Google and Perform Assertion from Google and API",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Web"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User get pokemon \"mewtwo\" and id from API",
  "keyword": "Given "
});
formatter.match({
  "location": "demo.steps.PokeSearchDef.userGetPokemonNameAndId(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User open Google Search homepage",
  "keyword": "When "
});
formatter.match({
  "location": "demo.steps.PokeSearchDef.userOpenGoogleSearchHomepage()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User input Search \"Mewtwo Wikipedia Indonesia\"",
  "keyword": "And "
});
formatter.match({
  "location": "demo.steps.PokeSearchDef.userInputSearch(java.lang.String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User press Enter Button",
  "keyword": "And "
});
formatter.match({
  "location": "demo.steps.PokeSearchDef.userPressEnterButton()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User Click The First Result",
  "keyword": "And "
});
formatter.match({
  "location": "demo.steps.PokeSearchDef.userClickTheFirstResult()"
});
formatter.result({
  "error_message": "org.openqa.selenium.ElementClickInterceptedException: element click intercepted: Element \u003ca jsname\u003d\"BKxS1e\" class\u003d\"gyPpGe\" role\u003d\"link\" tabindex\u003d\"0\" jsaction\u003d\"i3viod\" data-ved\u003d\"0ahUKEwjxjNGG24_pAhXUXCsKHdS8DdUQ67oDCAQ\"\u003e...\u003c/a\u003e is not clickable at point (66, 86). Other element would receive the click: \u003cdiv\u003e...\u003c/div\u003e\n  (Session info: chrome\u003d81.0.4044.129)\nBuild info: version: \u00273.141.59\u0027, revision: \u0027e82be7d358\u0027, time: \u00272018-11-14T08:17:03\u0027\nSystem info: host: \u0027dnid0661l01.local\u0027, ip: \u0027fe80:0:0:0:1cf4:b004:3e21:8c0a%en0\u0027, os.name: \u0027Mac OS X\u0027, os.arch: \u0027x86_64\u0027, os.version: \u002710.15.4\u0027, java.version: \u00271.8.0_251\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities {acceptInsecureCerts: false, browserName: chrome, browserVersion: 81.0.4044.129, chrome: {chromedriverVersion: 81.0.4044.69 (6813546031a4b..., userDataDir: /var/folders/1v/px0r5h4x0jz...}, goog:chromeOptions: {debuggerAddress: localhost:58008}, javascriptEnabled: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: MAC, platformName: MAC, proxy: Proxy(), setWindowRect: true, strictFileInteractability: false, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}, unhandledPromptBehavior: dismiss and notify, webauthn:virtualAuthenticators: true}\nSession ID: 4bfa24633340f269ba8c854e78bca3ca\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.createException(W3CHttpResponseCodec.java:187)\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:122)\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:49)\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:158)\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:552)\n\tat org.openqa.selenium.remote.RemoteWebElement.execute(RemoteWebElement.java:285)\n\tat org.openqa.selenium.remote.RemoteWebElement.click(RemoteWebElement.java:84)\n\tat demo.pages.GoogleSearchPages.clickFirstSearch(GoogleSearchPages.java:14)\n\tat demo.steps.PokeSearchDef.userClickTheFirstResult(PokeSearchDef.java:44)\n\tat ✽.User Click The First Result(file:///Users/shafiqn/IdeaProjects/Exam_Nauval-Shafiq/src/test/resources/features/WebAPIPokemon.feature:39)\n",
  "status": "failed"
});
formatter.step({
  "name": "User see article with title \"Mewtwo\"",
  "keyword": "And "
});
formatter.match({
  "location": "demo.steps.PokeSearchDef.userSeeArticleWithTitle(java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User Get the name \"Mewtwo\" and id \"25\" from Wikipedia",
  "keyword": "And "
});
formatter.match({
  "location": "demo.steps.PokeSearchDef.userGetTheNameAndIdFromWikipedia(java.lang.String,java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User perform Assertion pokemon \"Mewtwo\"",
  "keyword": "Then "
});
formatter.match({
  "location": "demo.steps.PokeSearchDef.userPerformAssertionPokemon(java.lang.String)"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "passed"
});
});