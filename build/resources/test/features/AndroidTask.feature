@Android_1
  Feature: Create to Do
    Scenario: User create to do list
      Given User is on to do list page
    @Android_2
  Feature: Check to Do
    Scenario: User check to do
      Given User is on to do list page
  @Android_3
  Feature: Uncheck to Do
    Scenario: User uncheck to do
      Given User is on to do list page
  @Android_4
  Feature: Filter to Do for "Active" only
    Scenario: User filter to do for active only
      Given User is on to do list page
  @Android_5
  Feature: Filter to Do for "Complete" only
    Scenario: User filter to do for complete only
      Given User is on to do list page
  @Android_6
  Feature: Refresh
    Scenario: User refresh page
      Given User is on to do list page
      When User click options on to do list
      And User click refresh page
      Then Page Displayed

    @Android_7
    Feature: Clear Complete to do
    Scenario: User check statistic
      Given User is on to do list page

    @Android_8
  Feature: Check Statistic
    Scenario: User check statistic
      Given User is on to do list page
