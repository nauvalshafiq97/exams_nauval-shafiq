@Web
Feature: Search Pokemon
  Scenario: Search Pikachu from Google and Perform Assertion from Google and API
    Given User get pokemon "pikachu" and id from API
    When User open Google Search homepage
    And User input Search "Pikachu Wikipedia Indonesia"
    And User press Enter Button
    And User Click The First Result
    And User see article with title "Pikachu"
    And User Get the name "Pikachu" and id "25" from Wikipedia
    Then User perform Assertion pokemon "Pikachu"


  Scenario: Search Charizard from Google and Perform Assertion from Google and API
    Given User get pokemon "charizard" and id from API
    When User open Google Search homepage
    And User input Search "Charizard Wikipedia Indonesia"
    And User press Enter Button
    And User Click The First Result
    And User see article with title "Charizard"
    And User Get the name "Charizard" and id "25" from Wikipedia
    Then User perform Assertion pokemon "Charizard"

  Scenario: Search Bulbasaur from Google and Perform Assertion from Google and API
    Given User get pokemon "bulbasaur" and id from API
    When User open Google Search homepage
    And User input Search "Bulbasaur Wikipedia Indonesia"
    And User press Enter Button
    And User Click The First Result
    And User see article with title "Bulbasaur"
    And User Get the name "Bulbasaur" and id "25" from Wikipedia
    Then User perform Assertion pokemon "Bulbasaur"

  Scenario: Search Mewtwo from Google and Perform Assertion from Google and API
    Given User get pokemon "mewtwo" and id from API
    When User open Google Search homepage
    And User input Search "Mewtwo Wikipedia Indonesia"
    And User press Enter Button
    And User Click The First Result
    And User see article with title "Mewtwo"
    And User Get the name "Mewtwo" and id "25" from Wikipedia
    Then User perform Assertion pokemon "Mewtwo"
